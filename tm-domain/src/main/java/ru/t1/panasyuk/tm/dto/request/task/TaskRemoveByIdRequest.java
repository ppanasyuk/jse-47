package ru.t1.panasyuk.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskRemoveByIdRequest(@Nullable final String id) {
        this.id = id;
    }

    public TaskRemoveByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}